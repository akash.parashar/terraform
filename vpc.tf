resource "aws_vpc" "opstree-vpc-2" {
    cidr_block = "172.28.0.0/16"
    aws_nat_gateway = true
    tags = {
      name = "opstree-1"
    }
}
resource "aws_subnet" "opstree-subnet-1" {
    vpc_id = aws_vpc.opstree-vpc-2.id
    cidr_block = "172.28.10.0/24"
    availability_zone = "us-east-2a"
    map_public_ip_on_launch = "true"
    tags = {
      name = "opstree-public-subnet-1"
    }
}
resource "aws_subnet" "opstree-subnet-2" {
    vpc_id = aws_vpc.opstree-vpc-2.id
    cidr_block = "172.28.11.0/24"
    availability_zone = "us-east-2a"
    map_public_ip_on_launch = "true"
    tags = {
      name = "opstree-public-subnet-2"
    }
}
resource "aws_subnet" "opstree-subnet-3" {
    vpc_id = aws_vpc.opstree-vpc-2.id
    cidr_block = "172.28.12.0/24"
    availability_zone = "us-east-2a"
    tags = {
      name = "opstree-private-subnet-1"
    }
}
resource "aws_subnet" "opstree-subnet-4" {
    vpc_id = aws_vpc.opstree-vpc-2.id
    cidr_block = "172.28.13.0/24"
    availability_zone = "us-east-2a"
    tags = {
      name = "opstree-private-subnet-2"
    }
}


