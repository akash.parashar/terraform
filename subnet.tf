resource "aws_vpc" "task1"  {
    cidr_block = "172.36.0.0/16"
    instance_tenancy = "default"
    enable_dns_hostnames = "true"

    tags = {
        Name = "task_VPC"
    }
}

resource "aws_subnet" "subnet1" {
    depends_on = [
        aws_vpc.task1,
    ]
    vpc_id = aws_vpc.task1.id
    cidr_block = "172.36.10.0/24"
    map_public_ip_on_launch = "true"
    tags = {
        Name = "task-subnet-public"
    }
}

resource "aws_subnet" "subnet2" {
    depends_on = [
        aws_vpc.task1,
    ]
    vpc_id = aws_vpc.task1.id
    cidr_block = "172.36.11.0/24"
    tags = {
        Name = "task-subnet-private"
    }
}

resource "aws_internet_gateway" "InternetGateway" {
    depends_on = [
        aws_vpc.task1,
    ]
    
    vpc_id = aws_vpc.task1.id
    tags = {
        Name = "Internet_Gateway"
    } 

}

resource "aws_route_table" "routeTable" {
    depends_on = [
        aws_internet_gateway.InternetGateway,
    ]
        
    vpc_id = aws_vpc.task1.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.InternetGateway.id 
    }
    tags = {
        Name = "Route_table"
    }

}

resource "aws_route_table_association" "a" {
    subnet_id = aws_subnet.subnet1.id
    route_table_id = aws_route_table.routeTable.id 
}

resource "aws_eip" "ip" {
    vpc = true
}

resource "aws_nat_gateway" "natgw" {
    allocation_id = aws_eip.ip.id
    subnet_id = aws_subnet.subnet1.id

    tags = {
    Name = "NAtgateway"
    }
}

resource "aws_route_table" "natroute" {
    vpc_id = aws_vpc.task1.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.natgw.id 
    }

    tags = {
        Name = "NatRoute"

    }
}

resource "aws_route_table_association" "associate" {
    subnet_id = aws_subnet.subnet2.id 
    route_table_id = aws_route_table.natroute.id 
}



